#pragma once

#include "ofMain.h"
#include "ofxRunway.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp, public ofxRunwayListener{
    
public:
    void setup();
    void update();
    void draw();
    void mouseDragged(int x, int y, int button);
    // Callback functions that process what Runway sends back
    void runwayInfoEvent(ofJson& info);
    void runwayErrorEvent(string& message);
    void turnOff(const void * sender, bool & value);
    void coco(const void * sender, bool & value);
    
    ofxRunway runway;
    ofxRunway runway2;
    ofImage send2Runway, getRunway;
    ofImage send2Runway2, getRunway2;
    
    
    ofxPanel gui;
    ofxPanel gui2;
    ofxToggle tt1;
    ofxToggle tt2;
    ofxToggle tt3;
    ofxToggle tt4;
    ofxToggle tt5;
    ofxToggle tt6;
    ofxToggle tt7;
    ofxToggle tt8;
    ofxToggle tt9;
    ofxToggle tt10;
    ofxToggle t1;
    ofxToggle t2;
    ofxToggle t3;
    ofxToggle t4;
    ofxToggle t5;
    ofxToggle t6;
    ofxToggle done;
    ofxIntSlider penSize;
    ofxIntSlider penSize2;
    ofImage landscape;
    
    ofFbo fbo;
    ofFbo fbo2;
    
    ofColor c;
    ofColor c2;
    bool finish;
    
};
