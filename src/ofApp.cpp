#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    //bool to control our finish button
    finish = false;
    //set background
    ofBackgroundHex(0x5fdbff);
    //gui setups
    gui.setup();
    gui2.setup();
    gui.add(t1.setup("Grass", false, 100));
    gui.add(t2.setup("Sky", false, 100));
    gui.add(t3.setup("Clouds", false, 100));
    gui.add(t4.setup("Sea", false, 100));
    gui.add(t5.setup("River", false, 100));
    gui.add(t6.setup("Tree", false, 100));
    gui.add(done.setup("Finished with Landscape", false, 100));
    gui.add(penSize.setup("Pen Size", 15, 0, 100));
    gui2.add(tt1.setup("Sandwich", false, 200));
    gui2.add(tt2.setup("BackPack", false, 200));
    gui2.add(tt3.setup("Wine Glass", false, 200));
    gui2.add(tt4.setup("Apple", false, 200));
    gui2.add(tt5.setup("Blanket", false, 200));
    gui2.add(tt6.setup("Person", false, 200));
    gui2.add(tt7.setup("Car", false, 200));
    gui2.add(tt8.setup("Boat", false, 200));
    gui2.add(tt9.setup("Bench", false, 200));
    gui2.add(tt10.setup("Fence", false, 200));
    gui2.add(penSize2.setup("Pen Size", 15, 0, 100));
    
    //Add event listeners to each toggle
    //This is to make the toggles work like radio buttons
    t1.addListener(this, &ofApp::turnOff);
    t2.addListener(this, &ofApp::turnOff);
    t3.addListener(this, &ofApp::turnOff);
    t4.addListener(this, &ofApp::turnOff);
    t5.addListener(this, &ofApp::turnOff);
    t6.addListener(this, &ofApp::turnOff);
    done.addListener(this, &ofApp::turnOff);
    tt1.addListener(this, &ofApp::coco);
    tt2.addListener(this, &ofApp::coco);
    tt3.addListener(this, &ofApp::coco);
    tt4.addListener(this, &ofApp::coco);
    tt5.addListener(this, &ofApp::coco);
    tt6.addListener(this, &ofApp::coco);
    tt7.addListener(this, &ofApp::coco);
    tt8.addListener(this, &ofApp::coco);
    tt9.addListener(this, &ofApp::coco);
    tt10.addListener(this, &ofApp::coco);
    
    //start the runway instance
    runway.setup(this,"http://localhost:8000");
    runway2.setup(this,"http://localhost:8001");
    runway.start();
    runway2.start();
    //slow it down to 30fps
    ofSetFrameRate(30);
    
    //allocate our outgoing and returned images
    send2Runway.allocate(1920/2, 1080, OF_IMAGE_COLOR);
    getRunway.allocate(1920/2, 1080, OF_IMAGE_COLOR);
    send2Runway2.allocate(1920/2, 1080, OF_IMAGE_COLOR);
    getRunway2.allocate(1920/2, 1080, OF_IMAGE_COLOR);
    
    //Allocate our first fbo object
    fbo.allocate(1920/2, 1080, GL_RGBA);
    fbo.begin();
    ofClear(255, 255, 255, 0);
    fbo.end();
    
}

//Updates our runway image that we get back.
void ofApp::update(){
    if(finish == false){
        //send our image to runway
        if(!runway.isBusy()){
            ofxRunwayData data;
            data.setImage("semantic_map", send2Runway, OFX_RUNWAY_JPG);
            runway.send(data);
        }
        //get our reculting image back
        runway.get("output", getRunway);
    }else{
        if(!runway2.isBusy()){
            ofxRunwayData data;
            data.setImage("semantic_map", send2Runway2, OFX_RUNWAY_JPG);
            runway2.send(data);
        }
        //get our reculting image back
        runway2.get("output", getRunway2);
    }
}

//Draw method that will take an incoming set of pixels,
//and prep it to get sent to runway. It will then grab the outcoming image and store that in
//a picture. Then draw it to the screen with opacity if needed.
void ofApp::draw(){
    
    if(finish == false){
        fbo.draw(0,0);
        ofPixels temp2;
        fbo.readToPixels(temp2);
        send2Runway.setFromPixels(temp2);
        
        
        if(getRunway.isAllocated()){
            ofSetColor(255); //set color to white so resulting image is the right color
            //getRunway.draw(1920/2, 0);
            ofImage test;
            test = getRunway.getPixels();
            test.draw(1920/2, 0);
            landscape =getRunway.getPixels();
        }
        
        gui.draw();
    }else{
        
        landscape.draw(1920/2, 0);
        fbo2.draw(0,0);
        ofPixels temp;
        fbo2.readToPixels(temp);
        send2Runway2.setFromPixels(temp);
        
        if(getRunway2.isAllocated()){
            //ofBackgroundHex(0x5fdbff);
            
            ofEnableAlphaBlending();
            ofSetColor(255,255,255,170); //set color to white so resulting image is the right color
            ofImage test;
            test = getRunway2.getPixels();
            test.draw(1920/2, 0);
            ofDisableAlphaBlending();
        }
        gui2.draw();
    }
}

//Runway info event
void ofApp::runwayInfoEvent(ofJson& info){
    ofLogNotice("ofApp::runwayInfoEvent") << info.dump(2);
}
// Runway error catcher
//--------------------------------------------------------------
void ofApp::runwayErrorEvent(string& message){
    ofLogNotice("ofApp::runwayErrorEvent") << message;
}

//Draws circles in the frame of the selected color.
void ofApp::mouseDragged(int x, int y, int button){
    
    if(finish == false){
        if(mouseX > 0 && mouseX < 1920/2){
            fbo.begin();
            ofSetColor(c);
            ofCircle(mouseX, mouseY, penSize);
            fbo.end();
        }
    }else{
        fbo2.begin();
        ofSetColor(c2);
        ofCircle(mouseX, mouseY, penSize2);
        fbo2.end();
    }
}
//Method to make the 2nd gui work like radio buttons rather than toggles
//meaning that the user cannot click more than 2 toggles at the same time
//since it would confuse my program.
//It also will allocate my 2nd fbo object at the end.
void ofApp::turnOff(const void * sender, bool & value){
    ofParameter<bool> * v = (ofParameter<bool> *)sender;
    const string & name = v->getName();
    if(finish == false){
        if(v->get() == true){
            if(name == "Grass"){
                t6 = false;
                t2 = false;
                t3 = false;
                t4 = false;
                t5 = false;
                c.setHex(0x1dc331);
            }else if(name == "Sky"){
                t1 = false;
                t6 = false;
                t3 = false;
                t4 = false;
                t5 = false;
                c.r = 95;
                c.g = 219;
                c.b = 255;
                c.setHex(0x5fdbff);
            }else if(name == "Clouds"){
                t1 = false;
                t2 = false;
                t6 = false;
                t4 = false;
                t5 = false;
                c.setHex(0xaaaaaa);
            }else if(name == "Sea"){
                t1 = false;
                t2 = false;
                t3 = false;
                t6 = false;
                t5 = false;
                c.setHex(0x363ea7);
            }else if(name == "River"){
                t1 = false;
                t2 = false;
                t3 = false;
                t4 = false;
                t6 = false;
                c.setHex(0x003996);
            }else if(name == "Tree"){
                t1 = false;
                t2 = false;
                t3 = false;
                t4 = false;
                t5 = false;
                c.setHex(0x8c682f);
            }else if(name == "Finished with Landscape"){
                finish = true;
                fbo2.allocate(1920/2, 1080, GL_RGBA);
                fbo2.begin();
                ofClear(255, 255, 255, 0);
                fbo2.end();
            }
        }
    }
    
    
}

//Method to make the 2nd gui work like radio buttons rather than toggles
//meaning that the user cannot click more than 2 toggles at the same time
//since it would confuse my program.
void ofApp::coco(const void * sender, bool & value){
    ofParameter<bool> * v = (ofParameter<bool> *)sender;
    const string & name = v->getName();
    if(finish == true){
        if(v->get() == true){
            if(name == "Sandwich"){
                tt2 = false;
                tt3 = false;
                tt4 = false;
                tt5 = false;
                tt6 = false;
                tt7 = false;
                tt8 = false;
                tt9 = false;
                tt10 = false;
                c2.setHex(0x623338);
            }else if (name == "BackPack"){
                tt1 = false;
                tt3 = false;
                tt4 = false;
                tt5 = false;
                tt6 = false;
                tt7 = false;
                tt8 = false;
                tt9 = false;
                tt10 = false;
                c2.setHex(0x344d00);
            }else if (name == "Wine Glass"){
                tt2 = false;
                tt1 = false;
                tt4 = false;
                tt5 = false;
                tt6 = false;
                tt7 = false;
                tt8 = false;
                tt9 = false;
                tt10 = false;
                c2.setHex(0x7e4497);
            }else if (name == "Apple"){
                tt2 = false;
                tt3 = false;
                tt1 = false;
                tt5 = false;
                tt6 = false;
                tt7 = false;
                tt8 = false;
                tt9 = false;
                tt10 = false;
                c2.setHex(0xbd8a64);
            }else if (name == "Blanket"){
                tt2 = false;
                tt3 = false;
                tt4 = false;
                tt1 = false;
                tt6 = false;
                tt7 = false;
                tt8 = false;
                tt9 = false;
                tt10 = false;
                c2.setHex(0x566464);
            }else if (name == "Person"){
                tt2 = false;
                tt3 = false;
                tt4 = false;
                tt1 = false;
                tt5 = false;
                tt7 = false;
                tt8 = false;
                tt9 = false;
                tt10 = false;
                c2.setHex(0xd60000);
            }else if (name == "Car"){
                tt2 = false;
                tt3 = false;
                tt4 = false;
                tt1 = false;
                tt6 = false;
                tt5 = false;
                tt8 = false;
                tt9 = false;
                tt10 = false;
                c2.setHex(0x018700);
            }else if (name == "Boat"){
                tt2 = false;
                tt3 = false;
                tt4 = false;
                tt1 = false;
                tt6 = false;
                tt7 = false;
                tt5 = false;
                tt9 = false;
                tt10 = false;
                c2.setHex(0x00009c);
            }else if (name == "Bench"){
                tt2 = false;
                tt3 = false;
                tt4 = false;
                tt1 = false;
                tt6 = false;
                tt7 = false;
                tt8 = false;
                tt5 = false;
                tt10 = false;
                c2.setHex(0x95b379);
            }else if (name == "Fence"){
                tt2 = false;
                tt3 = false;
                tt4 = false;
                tt1 = false;
                tt6 = false;
                tt7 = false;
                tt8 = false;
                tt9 = false;
                tt5 = false;
                c2.setHex(0x752b01);
            }
            
            
        }
    }
}

