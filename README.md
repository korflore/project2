Title: Painting a Picture

![Screenshot](/Screenshot.png)

I set out to make a piece that was not only (somewhat pretty) but also interactive. I saw and was inspired by the piece done by Gene Kogan called "Futurism". I wanted to remake what he made yet put my own spin on it and added a few more complexities. Thus Painting a Picture was born.

Youtube Link to show how quickly I made the above screenshot: https://youtu.be/lVdc-f7tn1A